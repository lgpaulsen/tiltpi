$(function () {
    var seriesOptions = [],
        seriesCounter = 0,
        names = ['SG', 'TEMP'];

    /**
     * Create the chart when all data is loaded
     * @returns {undefined}
     */
    function createChart() {

        Highcharts.stockChart('chart', {

            rangeSelector: {
				enabled:false
               },
			navigator: {
                enabled: false
            },
			scrollbar: {
                enabled: false
            },
			chart: {
                zoomType: 'x'
            },
			
          yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[3]
                    }
                },
                title: {
                    text: 'Temperature',
                    style: {
                        color: Highcharts.getOptions().colors[3]
                    }
                },
				opposite: true
            }, { // Secondary yAxis
                title: {
                    text: 'Specific gravity',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
		plotLines: [{
    color: 'green',
    width: 2,
    value: beerData.targetFG,
    dashStyle: 'dashdot',
    label: {
	text: 'Target FG ('+beerData.targetFG+')',
	textAlign: 'left'
	} 
  }],
				opposite: false
            }],
			
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                valueDecimals: 0,
                split: true
            },

            series:  seriesOptions
		   });
    }

    $.each(names, function (i, name) {
		$.getJSON('api.php?filename=' + name.toLowerCase() + '-c.json&batch=' + beerData.batchnr + '&callback=?',    function (data) {
			if (name == "TEMP") {
				var yAxis = 0;
				var color = 'rgba(247,163,92,0.20)';
				var id = "temp";
			}
			else {
				var yAxis = 1;
				var color = "#7cb5ec";
				var id = "temp";
				}
			seriesOptions[i] = {
                name: name,
                data: data,
				color: color,
				yAxis: yAxis,
				id: id
            };
            seriesCounter += 1;
            if (seriesCounter === names.length) {
                createChart();
            }
        });
    });
});
