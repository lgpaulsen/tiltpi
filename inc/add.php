<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add new batch</h4>
      	</div>
      	<div class="modal-body">

		<form class="form-horizontal" action="functions.php?add" method="post">
  			<div class="form-group">
    				<label class="control-label col-xs-3" for="batchnr">Batchnr:</label>
    				<div class="col-xs-9">
      					<input type="text" class="form-control" id="batch" name="batchnr" placeholder="Enter Batch number">
    					</div>
  				</div>
  			<div class="form-group">
    				<label class="control-label col-xs-3" for="name">Beer Name</label>
    				<div class="col-xs-9"> 
      					<input type="text" class="form-control" id="name" name="name" placeholder="Enter Beer name">
    					</div>
  				</div>
  			<div class="form-group">
    				<label class="control-label col-xs-3" for="type">Type</label>
    				<div class="col-xs-9">
      					<input type="text" class="form-control" id="type" name="type" placeholder="Enter Beer type">
    					</div>
  				</div>
 			<div class="form-group">
    				<label class="control-label col-xs-3" for="fg">Target FG:</label>
    				<div class="col-xs-9">
      					<input type="text" class="form-control" id="fg" name="fg" placeholder="Enter Target FG">
    					</div>
  				</div>
		<input id="submit" name="submit" type="submit" value="Add" class="btn btn-success">
        	<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      		</div>
    	</div>
		</form>
      </div>
    </div>
