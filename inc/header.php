<?php
$beers = json_decode(file_get_contents("DATA/batchdb"));
$tiltsdb = json_decode(file_get_contents("DATA/tiltsdb"));
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/tilt.ico">

    <title>TILT&trade;(pi)</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- HighStock support -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://code.highcharts.com/stock/highstock.js"></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>

  </head>
  <body>


<?php $reqfile = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);?>

<div class="site-wrapper">
      <div class="site-wrapper-inner">
        <div class="cover-container">
          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand"><a href="./"><img src="img/tilt.png" height="100px" alt="TILT&trade;"></a>TILT&trade;(pi) <span class="hidden-xs">- Beer fermentation</span></h3>
              <nav>
                <ul class="nav masthead-nav">
		<?php $query = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']); ?>
                <li <?php if($query == "") { echo ' class="active"'; }?>><a href="./">Batch</a></li>
		<li <?php if($query == "tilts.php") { echo ' class="active"'; }?>><a href="tilts.php">Tilts</a></li>
                <li <?php if($query == "about.php") { echo ' class="active"'; }?>><a href="about.php">About</a></li>
                </ul>
              </nav>
            </div>
          </div>

          <div class="inner cover">
