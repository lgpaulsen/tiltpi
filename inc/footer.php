 </div>
          <div class="mastfoot">
            <div class="inner hidden-sm hidden-xs">
              <p>Powered by <a href="http://getbootstrap.com/">Bootstrap</a>, <a href="https://getbootstrap.com/examples/cover/">Cover Template</a> and <a href="http://www.highcharts.com/">Highstocks</a> - Made by <a href="https://dev.n0ll.com">lgpaulsen</a>.</p>
            </div>
          </div>

        </div>
</div>
</div>

<!-- Modals -->
<?php
include("inc/add.php"); // Add modal
include("inc/start.php"); // start modal
?>



<!-- Botstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <!-- modal script -->
    <script language="javascript">
      
       $(document).ready(function(){
           $(".open-startmodal").click(function () { // Click to only happen on announce links
               $("#batchval").val($(this).data('id'));
         $('#startmodal').modal('show');
       });
    });

    </script>
  </body>
</html>

