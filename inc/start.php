<!-- Modal -->
<div id="startmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Start batch</h4>
      	</div>
      	<div class="modal-body">
		<form class="form" action="functions.php?start&" method="get">
			<input type="hidden" name="start" value="">
			<input type="hidden" name="batch" id="batchval" value=""> 
  			<div class="form-group">
    				<label class="control-label" for="batchnr">TILT:</label>
					<select name="uuid" class="selectpicker">
						<?php foreach($tiltsdb as $tilt) {
							echo "<option value=\"$tilt->UUID\">$tilt->color</option>"; 
							}
							?>
					</select>
  				</div>
		<input id="submit" name="submit" type="submit" value="Start" class="btn btn-success">
        	<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      		</div>
    	</div>
		</form>
      </div>
    </div>
