#!/bin/bash
echo "[*] Fetching latest updates, and upgrading"
apt-get -qq update				
apt-get -qq dist-upgrade -y 	
apt-get -qq -y install lighttpd 
apt-get -qq -y install php7.3-common php7.3-cgi php7.3
apt-get -qq -y install bluetooth bluez python-bluez
apt-get -qq -y install git

echo "[*] Enable php for Lighttpd"
lighty-enable-mod fastcgi-php
service lighttpd force-reload

mv /var/www/html /var/www/html.old
cd /var/www/

echo "[*] Fetching a fresh copy of TiltPI and run setup"
git clone --quiet https://bitbucket.org/lgpaulsen/tiltpi.git html
chown -R www-data:www-data html
cd html
./setup.sh
