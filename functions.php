<?php

// ERROR LOGGING
function errorlog($message) {
	$logging = "yes"; // yes or no to log errors to file
	$logfile = "DATA/errorlog";
	if($logging == "yes") {
		file_put_contents($logfile, $message, FILE_APPEND | LOCK_EX);
		}
	exit("ERROR: Something went wrong, check $logfile if debug is set to yes");
}


// File definitions
$batchdb_file = "DATA/batchdb";
$tiltsdb_file = "DATA/tiltsdb";


// Quick and dirty security, to make sure you can't write eg php til local files 
function playnice($arr) {
	foreach($arr as &$v) {
		if($v == "batchnr") {
			$v = intval($v);
			}
		elseif($v == "fg") {
			$v = intval($v);
			}
		else{
			 $v = preg_replace('/[^A-Za-z0-9\. -]/', '', $v);
			}
		}
	return $arr;
	}

if(!empty($_POST) or !empty($_GET)) {
$_POST = playnice($_POST);
$_GET = playnice($_GET);
} else {
	 errorlog("ERROR: Empty GET or POST objects");
}

// Add new batch
if(isset($_GET['add'])) {
   try {
	$batch=$_POST['batchnr'];
	$batchdb = json_decode(file_get_contents($batchdb_file));
	$batchdb->$batch = $_POST;
	$batchdb = json_decode(json_encode($batchdb),true);
        krsort($batchdb);
	file_put_contents($batchdb_file, json_encode($batchdb));
	header('Location: '.$_SERVER['HTTP_REFERER']);
	} 
   catch (Exception $e) {
	errorlog($e->getMessage());
  	}
}

// Delete batch
if(isset($_GET['del'])) {
   try {
	$batch = $_GET['batch'];
	$batchdb = json_decode(file_get_contents($batchdb_file));
     	unset($batchdb->$batch);
	file_put_contents($batchdb_file, json_encode($batchdb));
        header('Location: '.$_SERVER['HTTP_REFERER']);
	}
   catch (Exception $e) {
        errorlog($e->getMessage());
        }
}

// Start batch
if(isset($_GET['start'])) {
   try {
	$batch = $_GET['batch'];
	$uuid  = $_GET['uuid'];
	$batchdb = json_decode(file_get_contents($batchdb_file));
	$tiltsdb = json_decode(file_get_contents($tiltsdb_file));
	// Clean old instance of tilt use in batch db
	foreach($batchdb as &$b) {
		if($b->tilt == $uuid) { unset($b->tilt); }
		}
	$batchdb->$batch->tilt = $tiltsdb->$uuid->UUID;
	$tiltsdb->$uuid->BATCH = $batch;
	file_put_contents($batchdb_file, json_encode($batchdb));
	file_put_contents($tiltsdb_file, json_encode($tiltsdb));
	header('Location: '.$_SERVER['HTTP_REFERER']);
	}
   catch (Exception $e) {
        errorlog($e->getMessage());
        }
}

// Start batch
if(isset($_GET['stop'])) {
   try {
        $batch = $_GET['batch'];
        $batchdb = json_decode(file_get_contents($batchdb_file));
        $tiltsdb = json_decode(file_get_contents($tiltsdb_file));
	$uuid = $batchdb->$batch->tilt;
        unset($batchdb->$batch->tilt);
	unset($tiltsdb->$uuid->BATCH);
        file_put_contents($batchdb_file, json_encode($batchdb));
        file_put_contents($tiltsdb_file, json_encode($tiltsdb));
        header('Location: '.$_SERVER['HTTP_REFERER']);
        }
   catch (Exception $e) {
        errorlog($e->getMessage());
        }
}


?>
