import blescan
import sys
import time
import string
import json
import os.path
import bluetooth._bluetooth as bluez

dev_id = 0
timestamp = str(int(time.time()))
try:
        sock = bluez.hci_open_dev(dev_id)
except:
        print "error accessing bluetooth device..."
        sys.exit(1)
blescan.hci_le_set_scan_parameters(sock)
blescan.hci_enable_le_scan(sock)

## Timeout for socket if no beacons found
sock.settimeout(60)

## Value of 10, might need tweeking to if many bluetooth beacons is present
try:
	returnedList = blescan.parse_events(sock, 10)
except:
	print("No Beacons found.. exiting..")
  	sys.exit(0)

## Define data holder, load tiltsdb
DATA = {}
with open('DATA/tiltsdb') as tiltdb:
    tilts = json.load(tiltdb)

## Loop trough beacons, split and get UUID
for beacon in returnedList:
        arr = string.split(beacon, ",")
        UUID = arr[1]
        beacon = timestamp+","+beacon
        DATA[UUID] = beacon

for (uuid, data) in DATA.items():
    tiltIDs = {
        'a495bb10c5b14b44b5121370f02d74de' : 'RED',
        'a495bb20c5b14b44b5121370f02d74de' : 'GREEN',
        'a495bb30c5b14b44b5121370f02d74de' : 'BLACK',
        'a495bb40c5b14b44b5121370f02d74de' : 'PURPLE',
        'a495bb50c5b14b44b5121370f02d74de' : 'ORANGE',
        'a495bb60c5b14b44b5121370f02d74de' : 'BLUE',
        'a495bb70c5b14b44b5121370f02d74de' : 'YELLOW',
        'a495bb80c5b14b44b5121370f02d74de' : 'PINK',
        }
    ## Extract data from TILT Beacon
    if uuid in tiltIDs:
        arr = string.split(data, ",")
        timestamp=arr[0]
        MAC=arr[1]
        temp=arr[3]
        SG=arr[4]
        TX=arr[5]
        RSSI=arr[6]
        color=tiltIDs[uuid]
        ## Keep batchinfo, else update uuid in tiltsdb
        if uuid in tilts:
            if 'BATCH' in tilts[uuid]:
                BATCH=tilts[uuid]['BATCH']
                tilts[uuid] = {'UUID':uuid,'MAC':MAC,"color":color,"temp":temp,"SG":SG,"TX":TX,"RSSI":RSSI,"timestamp":timestamp,"BATCH":BATCH}
        else:
            tilts[uuid] = {'UUID':uuid,'MAC':MAC,"color":color,"temp":temp,"SG":SG,"TX":TX,"RSSI":RSSI,"timestamp":timestamp}
    else:
        print("Beacon found with UUID:"+uuid+" , but not defined as a TILT device")

## Write tiltsdb back to file
f = open('DATA/tiltsdb', 'w')
f.write(json.dumps(tilts))
f.close

## Add sleep to let filesystem free tiltsdb file
time.sleep( 5 )

## Writing data to disk
for (uuid, data) in DATA.items():
        ## Check if a TILT or not
        if uuid in tilts:
                f = open('DATA/batches/'+uuid, 'a')
                f.write(data+'\n')
                f.close()
                ## Write data to batchnummer
                try:
                        ##print("Writing to batch: "+tilts[uuid]['BATCH'])
                        f = open('DATA/batches/'+tilts[uuid]['BATCH'], 'a')
                        f.write(data+'\n')
                        f.close()
                except KeyError:
                        ## BATCH number not set for UUID, skipping
                        pass
