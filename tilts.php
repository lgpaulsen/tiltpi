<?php include("inc/header.php");?>

<h1> Tilts </H1>
<?php
if (count((array)$tiltsdb) == 0) {
        echo "<h3 class=\"text-warning\">No Tilts found</h3>";
        echo "Move RPi closer to TILT hydromenter, and activate it, and wait for 5 minuts";
}
else { ?>
<div style="text-align: left">
        <div class="table-responsive">
        <table class="table">
          <thead>
                <tr>
                        <th>Color</th>
                        <th class="hidden-xs">MAC</th>
                        <th class="hidden-xs">UUID</th>
			<th class="hidden-xs">TX</th>
			<th class="hidden-xs">RSSI</th>
			<th>SG</th>
			<th>Temp</th>
			<th>IN USE</th>
			<th>Last seeen</th>
                        </tr>
                </thead>
        <tbody>

<?php
foreach($tiltsdb as $tilt) {
	?>


	 	<tr>
                        <td><?php echo $tilt->color; ?></td>
                        <td class="hidden-xs"><?php echo $tilt->MAC;  ?></td>
			<td class="hidden-xs"><?php echo $tilt->UUID; ?></td>
			<td class="hidden-xs"><?php echo $tilt->TX; ?></td>
			<td class="hidden-xs">
				<?php
				$RSSI=abs($tilt->RSSI); 
				if     ($RSSI >= 90) 			{ $rssiclass="text-danger"; }
				elseif ($RSSI >= 80 && $RSSI < 90) 	{ $rssiclass="text-warning"; }
				else 					{ $rssiclass="text-success"; } 		
				echo "<p class=\"$rssiclass\">$tilt->RSSI</p>"; 
				?>
			</td>
			<td><?php echo $tilt->SG; ?></td>
			<td><?php echo round(($tilt->temp-32)/1.8,2); echo " C&deg"; ?></td>
			<td><?php if(!empty($tilt->BATCH)) { echo "Batch:<a href=\"./?batch=$tilt->BATCH\">$tilt->BATCH</a>"; }?></td>
			<?php 
				$seen=time()-$tilt->timestamp; 
				if     ($seen >= 600)                    { $seenclass="text-danger"; }
                        	elseif ($seen >= 300 && $seen < 600)     { $seenclass="text-warning"; }
                        	else                                     { $seenclass="text-success"; }
			?>
			<td><?php echo "<p class=\"$seenclass\">$seen s ago</p>"; ?></td>
                	</tr>
<?php } }?>

            </tbody>
        </table>



<?php include("inc/footer.php"); ?>
