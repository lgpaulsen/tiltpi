<?php
if(isset($_FILES['backup'])) {
	try {
		$tmpfile=$_FILES["backup"]["tmp_name"];
		move_uploaded_file($tmpfile,'/tmp/backup.tar.gz');
		// decompress from gz
		$p = new PharData('/tmp/backup.tar.gz');
		$p->decompress();
		// unarchive from the tar
		$phar = new PharData('/tmp/backup.tar');
		$phar->extractTo('/var/www/html',null, true);
		unlink('/tmp/backup.tar.gz');
		unlink('/tmp/backup.tar');
		}
	 catch (Exception $e) {
     		exit("ERROR: You sure you uploaded a TILTpi Backup file?");
		}
	}

header('Location: about.php?import');
?>
