# TILTpi#

A small project to replace [Tilt� Floating Wireless Hydrometer and Thermometer for Brewing](http://tilthydrometer.com/products/brewometer) app and google sheet implementation. It's build to run on a Raspberry PI v3 which has Bluetooth beacon support. But could run on any platform that support php/python and has a Bluetooth device that supports beacons. 

### Prerequisite ###

* Python
* Webserver w/ php support
* Bluetooth with beacon support



### Install ###
If you have a working system, with webserver with php, python and bluetooth
You can follow these steps to get up and running. 

* Download code from bitbucket 
* Run ./setup.sh 
* All done! 


If you do not have a system, I recommend a raspberry pi 3, and follow these three guides to get up and running.

* Get RASPBIAN STRETCH LITE from https://www.raspberrypi.org/downloads/raspbian/
* Writing img to SD CARD: https://www.raspberrypi.org/documentation/installation/installing-images/README.md
* Setting up wifi: https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md

When you have a running rasbian setup, enter the following commands to fix the rest!
~~~~
wget -O freshsetup.sh http://goo.gl/FK9jwg && sudo bash freshsetup.sh
~~~~

!! Security notice, You should never run code directly from the internet
!! Do it a three steps, download, review and execute! :)
~~~~
wget -O freshsetup.sh http://goo.gl/FK9jwg 
sudo bash freshsetup.sh
~~~~


### Screenshoots ###
![TILTpi Batch List](https://dev.n0ll.com/wordpress/wp-content/uploads/2017/11/tiltpi-batch-list.jpg)
![TILTpi Batch Overview](https://dev.n0ll.com/wordpress/wp-content/uploads/2017/11/tiltpi-batchoverview.jpg)
![TILTpi Tilts Overview](https://dev.n0ll.com/wordpress/wp-content/uploads/2017/11/tiltpi-tilts-list.jpg)
