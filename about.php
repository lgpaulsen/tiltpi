<?php include("inc/header.php");
if(isset($_GET['import'])){ echo '<div class="alert alert-success"><strong>Success!</strong> Backup imported sucessfully.</div>'; }
if(isset($_GET['clean'])){ echo '<div class="alert alert-success"><strong>Success!</strong> Cleaned '.htmlspecialchars($_GET['clean'], ENT_QUOTES, 'UTF-8').' Files</div>'; }
if(isset($_GET['reset'])){ echo '<div class="alert alert-success"><strong>Success!</strong> System has been reset, all DATA has been removed.</div>'; }
if(isset($_GET['update'])){ echo '<div class="alert alert-success"><strong>Success!</strong> TiltPi is now at latest version.</div>'; }
?>



<h1>About</h1>
<p>Small project as a replacment for google sheet from <a href="http://tilthydrometer.com/">TILT&trade;</a>, I do not have smart phone that can be nearby and upload too google sheets. 
Raspberry pi 3 supports bluetooth beacons and is a perfect little device you can place near your TILT&trade; fermentation.</p>
<p>I'm no programmer, just a hobby hacker that likes to get things working. I've tried to make the source easy to read, and since there
is very little data (execept for the bluetooth beacon) everything is stored in files with json encoding. If you want to improve, add 
or have any suggestion let me know. Create pull/push request, or add tickets to the bitbucket account.<br>

<h4 class="text-center"><a href="https://bitbucket.org/lgpaulsen/tiltpi">Tilt&trade;(pi) @ bitbucket</a></h4>

</p>

<h3>Calculations</h3>
<p>
During the testing, multiple deviation was observed. The trend however is stable. In order to compensate for the deviation found, OG value is an average of the first 6 measurements, and the SG is of the last 6 measurements. This will you get you a more "stable" value. To see if SG is stable, I've added a measurement too see how far back in time the SG has been stable. Since the SG variates easily 5 points between readings, and average for each hour to compensate for the spikes, and if there are no more than 2 point deviation, it will give you an estimate for how long the SG has been stable. (This is in testing, don't trust blindly...)</p>


<h3>Maintenance</h3>
<p>A collection of tools that might be handy if your having issues with the system</p>
<p>
	<a href="backup.php"><button type="button" class="btn btn-primary">Create Backup</button></a>
	<a href="#" data-toggle="modal" data-target="#import"><button type="button" class="btn btn-primary">Import Backup</button></a>
	<a href="update.php"><button type="button" class="btn btn-primary">Update</button></a>
	<a href="clean.php?clean"><button type="button" class="btn btn-warning">Clean</button></a>
	<a href="clean.php?reset"><button type="button" class="btn btn-danger">Reset</button></a>
</p>
<p>
	<b>Create Backup</b> will let you download a backup of all your DATA<br>
	<b>Import Backup</b> Let's your import DATA from a previous backup<br>
	<b>Update</b> Fetch latest version from BitBucket</br>
	<b>Clean</b> will delete all DATA files that is not linked to tiltdb or batchdb<br>
	<b>Reset</b> will delelete all DATA files, a fresh start<br>
</p>

<h3>License</h3>
<p>TILTpi is licensen under GNU GPLv3<br>
The componentes used are licenede under their respective licences. Everything is opensource, but HighCharts is only free for personal use. </p>


<div id="import" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Choose your backup, and upload to overwrite DATA <p>
	<form action="import.php" method="POST" enctype="multipart/form-data">
         <label class="btn btn-default btn-file">Select Backup<input type="file" name="backup" style="display: none;"/></label>
         <button type="submit" class="btn btn-primary">Submit</button>
      </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<?php include("inc/footer.php");?>
