<?php
// File definitions
$batchdb_file = "DATA/batchdb";
$tiltsdb_file = "DATA/tiltsdb";

// Get batches and tilts
$batchdb = json_decode(file_get_contents($batchdb_file));
$tiltsdb = json_decode(file_get_contents($tiltsdb_file));

$files=['tiltsdb','batchdb'];
foreach($batchdb as $batch) { $filepath="batches/".$batch->batchnr; array_push($files,$filepath); }
foreach($tiltsdb as $tilt)  { $filepath="batches/".$tilt->UUID;     array_push($files,$filepath); }
$data_path = realpath('DATA');
foreach($files as $key => $value) {
	$files[$key] = $data_path."/".$value; ## add full paths 
}


$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($data_path, RecursiveDirectoryIterator::SKIP_DOTS));
$cleaned=0;
foreach($objects as $name => $object){
    		if(isset($_GET['reset'])) {
			unlink($name);
		}
    		if(isset($_GET['clean'])) {
			if(!in_array($name,$files)) {
				unlink($name);
				$cleaned++;
			}
		}
}

if(isset($_GET['reset'])) {
// Create new strcuture for db files on reset
	$tfile=fopen($batchdb_file, "w") or die("unable to open file!");
	fwrite($tfile, "{}");
	fclose($tfile);
	$bfile=fopen($tiltsdb_file, "w") or die("unable to open file!");
	fwrite($bfile, "{}");
	fclose($bfile);
	header('Location: about.php?reset');
}
if(isset($_GET['clean'])) {
	$location="Location: about.php?clean=$cleaned";
	header($location);
}
?>
