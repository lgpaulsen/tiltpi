<?php
$filename = 'backup/TiltPI-Backup-' . date("d-m-Y_(G_i_s)") . ".tar";

// Create BACKUP DIRECTORY
if (!file_exists('backup')) {
    mkdir('backup', 0777, true);
}


// File definitions
$batchdb_file = "DATA/batchdb";
$tiltsdb_file = "DATA/tiltsdb";

// Get batches and tilts
$batchdb = json_decode(file_get_contents($batchdb_file));
$tiltsdb = json_decode(file_get_contents($tiltsdb_file));

$files=['tiltsdb','batchdb'];
foreach($batchdb as $batch) { $filepath="batches/".$batch->batchnr; array_push($files,$filepath); }
foreach($tiltsdb as $tilt)  { $filepath="batches/".$tilt->UUID;     array_push($files,$filepath); }

$data_path = 'DATA/';

try
{
    $archive = new PharData($filename);
    foreach($files as $file)
    {
        $archive->addFile( $data_path . $file );
    }
    $archive->compress(Phar::GZ);
} 
catch (Exception $e) 
{
    echo "Exception : " . $e;
}

unlink($filename);
$file=$filename.".gz";

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/x-tgz');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;
}

?>
